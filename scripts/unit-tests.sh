#!/bin/bash
cd ./ && npm test;
exit $?
# Fix ownership of output files
finish() {
    echo ${CURRENT_UID}
    # Fix ownership of output files
    chown -R ${CURRENT_UID} /nos/test-results
}
trap finish EXIT
