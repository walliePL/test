#!/bin/bash

if [ "$TIME_STAMP" == "" ]
then
  echo "Need to set env TIME_STAMP"
  exit -1
fi

if [ "$REVISION" == "" ]
then
  echo "Need to set env REVISION"
  exit -1
fi

if [ "$BUILD_NUMBER" == "" ]
then
  echo "Need to set env BUILD_NUMBER"
  exit -1
fi

docker-compose kill
docker-compose rm --f
docker-compose build --build-arg buildtime_variable='${TIME_STAMP}' --build-arg commitid_variable='${REVISION}' --build-arg buildnumber_variable='${BUILD_NUMBER}'