#!/bin/bash

export TIME_STAMP=$(date +%s)
export REVISION=local
export BUILD_NUMBER=0.0.1

${BASH_SOURCE%/*}/../buildImage.sh
docker-compose run --rm -e CURRENT_UID=$(id -u):$(id -g) --entrypoint "./scripts/unit-tests.sh" application
exit $?
