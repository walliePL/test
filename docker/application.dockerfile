FROM lambci/lambda:build-nodejs8.10

ARG buildtime_variable='N/A'
ENV buildtime=$buildtime_variable

ARG commitid_variable='N/A'
ENV commitid=$commitid_variable

ARG buildnumber_variable='N/A'
ENV buildnumber=$buildnumber_variable

WORKDIR /nos

COPY package.json ./

RUN npm install yarn

ENV PATH="/nos/node_modules/.bin:${PATH}"

RUN yarn install --pure-lockfile

COPY code ./code
COPY scripts ./scripts
COPY jest.config.json ./jest.config.json

ENTRYPOINT ["/bin/bash"]
