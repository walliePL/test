'use strict';

module.exports.sendEmailHandler = function(event, context, callback, AWSMock) {
 

  ses.sendEmail(emailParams, function (err, data) {
    // Set response headers to enable CORS (Cross-Origin Resource Sharing)
    const headers = {
      "Access-Control-Allow-Origin": "",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Headers": "Cache-Control,Content-Type,X-Api-Key",
      "Access-Control-Allow-Methods": "OPTIONS,POST",
    };
    if (err) {
      console.log(err, err.stack);
      const response = {
        statusCode: 500,
        headers: headers,
        body: JSON.stringify(err)
      };
      callback(null, response);
    } else {
      console.log(data);
      const response = {
        statusCode: 200,
        headers: headers,
        body: JSON.stringify({message: 'Ok'}),
      };
      callback(null, response);
    }
  });
}